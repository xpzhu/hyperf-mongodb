<?php

declare(strict_types=1);

namespace Hyperf\Mongodb\Pool;

use Hyperf\Di\Container;
use Psr\Container\ContainerInterface;
use Swoole\Coroutine\Channel;

class PoolFactory
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Channel[]
     */
    protected $pools = [];

    /**
     * PoolFactory constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * 获取连接池
     * @param string $name
     * @return MongoDBPool
     */
    public function getPool(string $name): MongoDBPool
    {
        if (isset($this->pools[$name])) {
            return $this->pools[$name];
        }

        if ($this->container instanceof Container) {
            $pool = $this->container->make(MongoDBPool::class, ['name' => $name]);
        } else {
            $pool = new MongoDBPool($this->container, $name);
        }
        return $this->pools[$name] = $pool;
    }
}